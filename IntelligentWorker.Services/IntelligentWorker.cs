﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntelligentWorker.Services.Dto;
using IntelligentWorker.Services.Interfaces;
using IntelligentWorker.Services.Processes;

namespace IntelligentWorker.Services
{
	public class IntelligentWorker
	{
		public static async void DoWork()
		{
			IProcess currentProcess = new TestProcess(-5);
			for (var i = 0; i < 100; i++)
			{
				try
				{
					var result = await currentProcess.Execute();
					var currentParameter = (currentProcess.CurrentParameterIndex + 1) > currentProcess.Parameters.Count() - 1 ? 0 : currentProcess.CurrentParameterIndex + 1;
					if (currentProcess.Results.Any())
					{
						Console.ForegroundColor = ConsoleColor.Blue;
						if (result.Margin < currentProcess.Results.Last().Margin)
						{
							//Console.WriteLine("DoWork: CLOSER");
						}
						else
						{
							//Console.WriteLine("DoWork: FURTHER");
						}
						Console.ForegroundColor = ConsoleColor.White;
					}
					if (result.Margin < 1 - currentProcess.TargetMargin)
					{
						Console.ForegroundColor = ConsoleColor.DarkYellow;
						//Console.WriteLine($"DoWork: Parameter Modify {currentProcess.Parameters[currentParameter].Name} -> {currentProcess.Parameters[currentParameter].CurrentState}");
						currentProcess.Parameters[currentParameter].Increment();
						currentProcess.CurrentParameterIndex = currentParameter;
						//Console.WriteLine("DoWork: Paramater Increment()");
						//Console.WriteLine($"DoWork: Parameter Modify {currentProcess.Parameters[currentParameter].Name} -> {currentProcess.Parameters[currentParameter].CurrentState}");
					}
					else if(result.Margin > 1 + currentProcess.TargetMargin)
					{
						//Console.ForegroundColor = ConsoleColor.DarkCyan;
						//Console.WriteLine($"DoWork: Parameter Modify {currentProcess.Parameters[currentParameter].Name} -> {currentProcess.Parameters[currentParameter].CurrentState}");
						currentProcess.Parameters[currentParameter].Decrement();
						currentProcess.CurrentParameterIndex = currentParameter;
						//Console.WriteLine("DoWork: Paramater Decrement()");
						//Console.WriteLine($"DoWork: Parameter Modify {currentProcess.Parameters[currentParameter].Name} -> {currentProcess.Parameters[currentParameter].CurrentState}");
					}
					else
					{
						if (result.Success)
						{
							Console.ForegroundColor = ConsoleColor.Green;
							//Console.WriteLine("DoWork: Optimized");
						}
						else
						{
							Console.ForegroundColor = ConsoleColor.DarkRed;
							//Console.WriteLine("DoWork: Failed");
							var currentResults = currentProcess.Results.ToList();
							currentResults.Add(result);
							currentProcess.Results = currentResults;
							throw new Exception();
						}
					}
					var results = currentProcess.Results.ToList();
					results.Add(result);
					currentProcess.Results = results;
				}
				catch (Exception e)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					//Console.WriteLine("");
					//Console.WriteLine($"Exception : Message -> {e.Message}");
					//Console.WriteLine($"Exception : StackTrace -> {e.StackTrace}");
					//Console.WriteLine("");
					// Parameters went wrong
					var currentParameter = (currentProcess.CurrentParameterIndex + 1) > currentProcess.Parameters.Count() - 1 ? 0 : currentProcess.CurrentParameterIndex + 1;
					var parameter = currentProcess.Parameters[currentParameter];
					//Console.WriteLine($"Exception : Parameter Value -> {parameter.CurrentState}");
					if (!currentProcess.Results.Any())
					{
						currentProcess.Results = new List<ProcessResult>
						{
							new ProcessResult
							{
								Success = false,
								Parameters = currentProcess.Parameters
							}
						};
					}
					var lastResult = currentProcess.Results.Last();
					var lastParameter = lastResult.Parameters[currentParameter];
					//Console.WriteLine($"Exception : Parameter History Value -> {lastParameter.CurrentState}");
					if (parameter.CurrentState < lastParameter.CurrentState || parameter.CurrentState == lastParameter.CurrentState)
					{
						//Console.WriteLine("Exception: Paramater Increment()");
						currentProcess.Parameters[currentParameter].Increment();
						currentProcess.CurrentParameterIndex = currentParameter;
					}
					else if (parameter.CurrentState > lastParameter.CurrentState)
					{
						//Console.WriteLine("Exception: Paramater Decrement()");
						currentProcess.Parameters[currentParameter].Decrement();
						currentProcess.CurrentParameterIndex = currentParameter;
					}
				}
				finally
				{
					Console.ForegroundColor = ConsoleColor.White;
				}
			}
			var x = currentProcess;
			Console.WriteLine(",Awaiter,Multiplier,Margin");
			for(var i = 0; i < x.Results.Count(); i++)
			{
				Console.WriteLine($"{i},{x.Results[i].Parameters[0].CurrentState},{x.Results[i].Parameters[1].CurrentState},{x.Results[i].Margin}");
			}
		}
	}
}
