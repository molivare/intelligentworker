﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IntelligentWorker.Services.Dto;

namespace IntelligentWorker.Services.Interfaces
{
	public interface IProcess
	{
		int CurrentParameterIndex { get; set; }
		IList<IParameter> Parameters { get; }
		Task<ProcessResult> Execute();
		decimal GetMargin();
		IList<ProcessResult> Results { get; set; }
		decimal TargetMargin { get; }
	}
}
