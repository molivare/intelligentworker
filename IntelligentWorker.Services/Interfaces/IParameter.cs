﻿namespace IntelligentWorker.Services.Interfaces
{
	public interface IParameter
	{
		int CurrentState { get; }
		void Decrement();
		void Increment();
		string Name { get; }
	}
}
