﻿using System;
using System.Collections.Generic;
using IntelligentWorker.Services.Interfaces;

namespace IntelligentWorker.Services.Dto
{
	public class ProcessResult
	{
		public decimal Margin { get; set; }
		public IList<IParameter> Parameters { get; set; }
		public Guid ProcessId { get; }
		public bool Success { get; set; }
	}
}
