﻿using IntelligentWorker.Services.Interfaces;

namespace IntelligentWorker.Services.Models
{
	public class IntegerParameter : IParameter
	{
		#region Constructors

		public IntegerParameter(string name)
		{
			CurrentState = 100;
			Name = name;
		}

		public IntegerParameter(string name, int value)
		{
			CurrentState = value;
			Name = name;
		}

		#endregion

		public int CurrentState { get; private set; }

		public string Name { get; private set; }

		public void Decrement()
		{
			CurrentState--;
		}

		public void Increment()
		{
			CurrentState++;
		}
	}
}
