﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using IntelligentWorker.Services.Dto;
using IntelligentWorker.Services.Interfaces;
using IntelligentWorker.Services.Models;

namespace IntelligentWorker.Services.Processes
{
	public class TestProcess : IProcess
	{
		private decimal _elapsedMs;
		private readonly IList<IParameter> _parameters;
		private IList<ProcessResult> _results;
		private readonly decimal _target = 200;

		#region Constructors

		public TestProcess()
		{
			_elapsedMs = 0;
			_parameters = new List<IParameter>
			{
				new IntegerParameter("awaiter")
			};
			_results = new List<ProcessResult>();
		}

		public TestProcess(int parameterValue)
		{
			_elapsedMs = 0;
			_parameters = new List<IParameter>
			{
				new IntegerParameter("awaiter", parameterValue),
				new IntegerParameter("multiplier", 3)
			};
			_results = new List<ProcessResult>();
		}

		#endregion

		public int CurrentParameterIndex { get; set; }

		public IList<IParameter> Parameters => _parameters;

		public decimal GetMargin()
		{
			return (_elapsedMs / _target);
		}

		public decimal TargetMargin => 0.1M;

		public IList<ProcessResult> Results
		{
			get
			{
				return _results;
			}
			set
			{
				_results = value;
			}
		}

		public async Task<ProcessResult> Execute()
		{
			var watch = Stopwatch.StartNew();
			var awaiter = _parameters.First(x => x.Name == "awaiter");
			var multiplier = _parameters.First(x => x.Name == "multiplier");
			var result = new ProcessResult
			{
				Success = false,
				Parameters = new List<IParameter>
				{
					new IntegerParameter("awaiter", awaiter.CurrentState),
					new IntegerParameter("multiplier", multiplier.CurrentState),
				}
			};

			if (awaiter.CurrentState == -1)
			{
				throw new ArgumentException("Argument would cause infinite wait.");
			}

			var randomInt = new Random().Next();
			var waitValue = awaiter.CurrentState * multiplier.CurrentState;
			//Console.WriteLine($"Execute: Awaiting {waitValue} ms");
			await Task.Delay(waitValue);

			if (randomInt % 3 == 0)
			{
				//Console.WriteLine($"Execute: Awaiting extra 50 ms");
				//await Task.Delay(50);
			}
			watch.Stop();
			_elapsedMs = watch.ElapsedMilliseconds;
			result.Margin = GetMargin();
			result.Success = true;

			return result;
		}
	}
}
