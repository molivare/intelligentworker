﻿using System;

namespace IntelligentWorker
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Services.IntelligentWorker.DoWork();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			//Console.WriteLine("completed");
			Console.ReadLine();
		}
	}
}
